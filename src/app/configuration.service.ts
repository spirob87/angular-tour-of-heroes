import { Injectable } from '@angular/core';
import {environment} from "../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class ConfigurationService {

  private config;

  constructor() {
    this.config = Object.assign({}, environment);
  }

  public getAll() {
    return this.config;
  }

  public getByName(name : string) {
    return this.config[name];
  }

  public createOrUpdateConfig(key : string, value : void) {
    this.config[key] = value;
  }

  public delete(key: string) {
    delete this.config[key];
  }
}
