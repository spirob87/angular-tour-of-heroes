import {Observable} from "rxjs";

export interface IStorage {
  select(key: string, defaultValue: any, localOrSession : boolean): Observable<any>;
  set(key: string, value: any, localOrSession : boolean): void;
  remove(key: string, localOrSession : boolean): void;
}
