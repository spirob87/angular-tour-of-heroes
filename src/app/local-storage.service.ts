import { Injectable } from '@angular/core';
import {IStorage} from "./interface/i.local.storage";
import {BehaviorSubject, Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService implements IStorage {

  protected subjectsLocal: {[key: string]: BehaviorSubject<any>} = {};
  protected subjectsSession: {[key: string]: BehaviorSubject<any>} = {};

  select(key: string, defaultValue: any = null, localOrSession : boolean): Observable<any> {

    var storage = localOrSession ? window.localStorage : window.sessionStorage;
    var subjLocOrSess = localOrSession ? this.subjectsLocal : this.subjectsSession;

    if (subjLocOrSess.hasOwnProperty(key)) {
      return subjLocOrSess[key];
    }

    if (!storage.getItem(key) && defaultValue) {
      storage.setItem(key, JSON.stringify(defaultValue));
    }

    const value = storage.getItem(key) ? JSON.parse(storage.getItem(key)) : defaultValue;

    return subjLocOrSess[key] = new BehaviorSubject(value);
  }

  set(key: string, value: any, localOrSession : boolean): void {

    var storage = localOrSession ? window.localStorage : window.sessionStorage;
    var subjLocOrSess = localOrSession ? this.subjectsLocal : this.subjectsSession;

    storage.setItem(key, JSON.stringify(value));

    if (subjLocOrSess.hasOwnProperty(key)) {
      subjLocOrSess[key].next(value);
    }
  }

  remove(key: string, localOrSession : boolean): void {

    var storage = localOrSession ? window.localStorage : window.sessionStorage;
    var subjLocOrSess = localOrSession ? this.subjectsLocal : this.subjectsSession;

    storage.removeItem(key);

    if (subjLocOrSess.hasOwnProperty(key)) {
      subjLocOrSess[key].next(null);
    }
  }
}
