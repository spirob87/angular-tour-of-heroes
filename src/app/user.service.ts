import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {User} from "./entities/user";
import {ConfigurationService} from "./configuration.service";

@Injectable({ providedIn: 'root' })
export class UserService {
  constructor(private configService: ConfigurationService, private http: HttpClient) { }

  getAll() {
    return this.http.get<User[]>(`${this.configService.getByName("apiUrl")}/users`);
  }

  getById(id: number) {
    return this.http.get(`${this.configService.getByName("apiUrl")}/users/${id}`);
  }

  register(user: User) {
    return this.http.post(`${this.configService.getByName("apiUrl")}/users/register`, user);
  }

  update(user: User) {
    return this.http.put(`${this.configService.getByName("apiUrl")}/users/${user.id}`, user);
  }

  delete(id: number) {
    return this.http.delete(`${this.configService.getByName("apiUrl")}/users/${id}`);
  }
}
