import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UtilsServiceService {

  constructor() { }

  public findAllElementsWithAttrName(attributeName : string) : NodeListOf<Element> {
    return document.querySelectorAll("[" + attributeName + "]")
  }
}
